package nl.lolmewn.itemmanager.items;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import nl.lolmewn.itemmanager.actions.Actionable;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Lolmewn
 */
public class ManagedItem implements Actionable {
    
    private final ItemStack stack;
    private Optional<Actionable> onClickAction = Optional.empty();
    
    public ManagedItem(ItemStack stack) {
        this.stack = stack;
    }
    
    public ManagedItem(Material material) {
        this(new ItemStack(material));
    }
    
    public ItemStack getItemStack() {
        return stack;
    }
    
    public ManagedItem setItemName(String name) {
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        this.stack.setItemMeta(meta);
        return this;
    }
    
    public ManagedItem setItemDescription(List<String> description) {
        ItemMeta meta = stack.getItemMeta();
        description.stream().map(str -> ChatColor.translateAlternateColorCodes('&', str));
        meta.setLore(description);
        this.stack.setItemMeta(meta);
        return this;
    }
    
    public ManagedItem setItemDescription(String... description) {
        return this.setItemDescription(Arrays.asList(description));
    }
    
    public ManagedItem setOnClickAction(Actionable action) {
        this.onClickAction = Optional.of(action);
        return this;
    }
    
    @Override
    public void perform(Player player) {
        onClickAction.ifPresent(action -> action.perform(player));
    }
    
    @Override
    public String toString() {
        return "ManagedItem{" + "stack=" + stack + ", onClickAction=" + onClickAction + '}';
    }
    
}
