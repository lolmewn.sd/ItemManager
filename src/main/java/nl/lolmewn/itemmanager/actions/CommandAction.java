package nl.lolmewn.itemmanager.actions;

import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn
 */
public class CommandAction implements Actionable {

    private final String command;

    public CommandAction(String command) {
        this.command = command;
    }
    
    @Override
    public void perform(Player player) {
        player.chat(command);
    }

}
