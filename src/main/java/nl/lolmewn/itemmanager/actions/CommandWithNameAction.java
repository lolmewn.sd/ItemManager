
package nl.lolmewn.itemmanager.actions;

import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn
 */
public class CommandWithNameAction implements Actionable {

    private final String command;

    public CommandWithNameAction(String command, String player) {
        this.command = String.format(command, player);
    }
    
    @Override
    public void perform(Player player) {
        player.chat(command);
    }

}
