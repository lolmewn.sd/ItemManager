
package nl.lolmewn.itemmanager.actions;

import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn
 */
public interface Actionable {
    
    public void perform(Player player);

}
