package nl.lolmewn.itemmanager.inv;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.itemmanager.items.ManagedItem;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Lolmewn
 */
public class ManagedInventory implements Listener {
    
    private final Plugin plugin;
    private final Inventory inventory;
    private final Map<Integer, ManagedItem> items = new HashMap<>();
    
    public ManagedInventory(Plugin plugin, String inventoryName) {
        this.plugin = plugin;
        this.inventory = plugin.getServer().createInventory(null, 54, colorise(inventoryName));
    }
    
    private void startListening() {
        plugin.getLogger().info("Starting to listen for events...");
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
    
    private void stopListening() {
        plugin.getLogger().info("Stopping listeners...");
        for (Method method : getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(EventHandler.class)) {
                for (Class<?> clazz : method.getParameterTypes()) {
                    if (Event.class.isAssignableFrom(clazz)) {
                        plugin.getLogger().info("Stopping " + clazz.getSimpleName() + " listener");
                        try {
                            HandlerList list = (HandlerList) clazz.getMethod("getHandlerList").invoke(null);
                            list.unregister(this);
                        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                            Logger.getLogger(ManagedInventory.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
    }
    
    public void open(Player player) {
        if (player.hasMetadata("managed-inventory")) {
            // Currently one open, better close it first
            ((ManagedInventory) player.getMetadata("managed-inventory").get(0).value()).close(player);
            plugin.getServer().getScheduler().runTaskLater(plugin, () -> open(player), 1);
            return;
        }
        if (inventory.getViewers().isEmpty()) {
            startListening();
        }
        plugin.getLogger().info("Opening inventory " + inventory.getTitle() + " for " + player.getName());
        player.setMetadata("managed-inventory", new FixedMetadataValue(plugin, this));
        player.openInventory(inventory);
    }
    
    public void close(Player player) {
        plugin.getLogger().info("Closing inventory " + inventory.getTitle() + " for " + player.getName());
        player.closeInventory();
        player.removeMetadata("managed-inventory", plugin);
        if (inventory.getViewers().isEmpty()) {
            stopListening();
        }
    }
    
    @EventHandler
    protected void onInventoryClose(InventoryCloseEvent event) {
        if (!event.getInventory().equals(this.inventory)) {
            return;
        }
        event.getPlayer().removeMetadata("managed-inventory", plugin);
        if (event.getInventory().getViewers().size() == 1) {
            stopListening();
        }
    }
    
    @EventHandler
    protected void onPlayerQuit(PlayerQuitEvent event) {
        onInventoryClose(new InventoryCloseEvent(event.getPlayer().getOpenInventory()));
    }
    
    public void addItem(ManagedItem item) {
        int slot = getFirstFreeSlot();
        if (slot != -1) {
            setItem(slot, item);
        }
    }
    
    public int getFirstFreeSlot() {
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null) {
                return i;
            }
        }
        return -1;
    }
    
    public int getLastFreeSlot() {
        for (int i = inventory.getSize() - 1; i >= 0; i--) {
            if (inventory.getItem(i) == null) {
                return i;
            }
        }
        return -1;
    }
    
    public void setItem(int slot, ManagedItem item) {
        plugin.getLogger().info(String.format("Setting pos %d to item %s", slot, item.toString()));
        this.inventory.setItem(slot, item.getItemStack());
        this.items.put(slot, item);
    }
    
    public Optional<ManagedItem> getItemAt(int index) {
        return Optional.ofNullable(items.get(index));
    }
    
    @EventHandler
    public void onItemTake(InventoryClickEvent event) {
        if (event.getRawSlot() >= event.getInventory().getSize()) {
            return;
        }
        event.setCancelled(true);
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }
        plugin.getLogger().info(String.format("[Inv] Click type: %s", event.getClick().name()));
        plugin.getLogger().info(String.format("[Inv] Action type: %s", event.getAction().name()));
        if (this.items.containsKey(event.getSlot())) {
            this.items.get(event.getSlot()).perform((Player) event.getWhoClicked());
        }
    }
    
    private String colorise(String string) {
        return string == null ? null : ChatColor.translateAlternateColorCodes('&', string);
    }
    
    public List<HumanEntity> getViewers() {
        return inventory.getViewers();
    }
    
    public void consume(ManagedInventory other) {
        this.inventory.clear();
        if (other == null) {
            return;
        }
        other.items.forEach((slot, item) -> {
            setItem(slot, item);
        });
        other.getViewers().stream().filter(e -> e instanceof Player).map(e -> (Player) e).forEach(this::open);
    }
    
    public void clear() {
        consume(null);
    }
    
}
